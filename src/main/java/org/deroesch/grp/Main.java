package org.deroesch.grp;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class Main {

	/*
	 * Parsing JSON in Java is painful.
	 */
	public static void main(String[] args) throws IOException {
		
		String json = new String(Files.readAllBytes(Paths.get("response.json")));
		
		// From a string...
		JsonElement e = JsonParser.parseString(json);
		
		// To an object...
		JsonObject obj = e.getAsJsonObject();
		
		/// And finally we can extract a value.
		String value = obj.get("sha").getAsString();

		// Print
		System.out.println("Expression: " + e);
		System.out.println("SHA Value : " + value);
	}
}
